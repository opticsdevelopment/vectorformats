FROM alpine:3.9 as builder
RUN apk add --no-cache py-setuptools
WORKDIR /out/
COPY vectorformats/ vectorformats/
COPY doc/ doc/
COPY setup.py MANIFEST.in README.txt ./
RUN python setup.py sdist

FROM busybox
WORKDIR /out/
COPY --from=builder /out/dist/ /out/dist/
RUN ls -laR /out/
